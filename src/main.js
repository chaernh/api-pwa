import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import 'bootstrap/dist/css/bootstrap.min.css'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import * as firebase from 'firebase'

Vue.config.productionTip = false
// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyDXUYfx2_UGzNvltWXCJud9wsnliDAMWUI',
  authDomain: 'vue-api-72471.firebaseapp.com',
  databaseURL: 'https://vue-api-72471.firebaseio.com',
  projectId: 'vue-api-72471',
  storageBucket: 'vue-api-72471.appspot.com',
  messagingSenderId: '633803929025',
  appId: '1:633803929025:web:137a2541298a455b14ab40',
  measurementId: 'G-1RZW64S8KS'
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig)
firebase.analytics()

firebase.auth().onAuthStateChanged(user => {
  store.dispatch('fetchUser', user)
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
